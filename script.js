var app = new Vue({
	el:"#app",
	data:{
		roles:['Developer', 'Climber', 'System Admin', 'Cyclist', 'Database Admin', 'DevOps Engineer', 'Architect', 'Drummer', 'Hoosier'],
		role:0,
		project0:{
				name: "crater.sh",
                link: 'https://dev.crater.sh',
				usage: "Containerization automation script sharing site",
				img:"./img/crater.svg",
				status:"Development",
				statusText: "text-warning",
				faIcon: "fa-node-js",
				langFrame:["Typescript", "Javascript", "MongoDB", "MySQL", "Bash", "Kubernetes"],
                snippetLang:"Typescript ",
				snippet:`
userRouter.post('/', async (req: Request, res: Response) => {
    // create user
    const payload:CreateUserNoSalt = req.body
    // converts password to salt & hashed values to return CreateUserDTO type
    const saltHashPayload = await userController.createUserSaltHash(payload)
    const results = await userController.create(saltHashPayload)
    // creates random pixelart and sends to blob storage
    if (results != undefined){
        const pixelGen = generateImage()
        const pixel = new BlobObject('user', \`\${results.id}.png\`, pixelGen.size, pixelGen.buffer) 
        pixel.upload()

        const id = await userController.setCookie(payload.username)
        res.cookie("loginToken", id, {signed: true})
        return res.status(200).send(results)
    }
    return res.status(400).send('{"Error": "could not create user"}')
})
`
		},
		projects:[
			{
				name: "Linux Master Class",
                link: 'https://jdogg.club',
				usage: "Linux Teaching Blog",
				img:"./img/lmc.svg",
				status:"Production",
				statusText: "text-success",
				faIcon: "fa-markdown",
				langFrame:["Toml", "HTML", "Hugo"],
                snippetLang:"Markdown",
				snippet:`
---
title: "Home"
date: 2020-11-09T22:18:24-05:00
draft: true
---
# Linux Master Class
>The how and why of the Linux operating System
---

## Philosophy
>Why a Linux Doc site?

Linux Tutorials suffer from being too simple or too complicated.

## Where to start?

Go to the [Modules Tab](./modules/) to get started!
\`\`\`sh	
	user@linux:~$ echo 'Hello, Linux!'
	Hello, Linux!
\`\`\`
![Linux Word Cloud](/linuxWordCloud.png)
			`
			},
			{
				name: "Rooster",
                link: 'https://gitlab.com/jaredamcdon/python-projects/-/tree/master/stock-analytics',
				usage: "Stock Analytics Webapp",
				img: "./img/rooster.svg",
				status:"Production",
				statusText: "text-success",
				faIcon: "fa-python",
				langFrame:["Python3", "MySQL", "Javascript", "Flask", "Vue"],
                snippetLang:"Python",
				snippet:`
to_return = []
if self.table_data[0][3] == True or self.table_data[0][3] == False:
    data = req.status_code
    if data == self.table_data[0][2]:
        code = True
    else:
        code = False
    to_return.append([self.table_data[0][0],self.table_data[0][1],code])
    # ^^this is used for checking for nager.date / similar
else:
    for call_var in self.table_data:
        data = req.json()
        if isinstance(data, str):
             data = json.loads(data)
        if call_var[3] == 'yank':
            if isinstance(call_var[2], list):
                for i in call_var[2]:
                    data = data[i]`
			},
			{
				name: "Reverse Proxy",
				usage: "Dedicated functional reverse proxy as code",
				status:"Production",
				img:"./img/nixos.png",
				statusText: "text-success",
				faIcon: "fa-linux",
				langFrame:["Nix", "NginX", "NixOS"],
                snippetLang:"nix",
				snippet:`
  services.nginx = {
      enable = true;
      virtualHosts."www.jdogg.club jdogg.club lmc.jdogg.club" = {
        http2 = false;
        onlySSL = true;
        sslCertificateKey = "/etc/nginx/jdogg.club.key";
        sslCertificate = "/etc/nginx/jdogg.club.pem";
  
        locations = {
          "/" = {
            proxyPass = "http://192.168.3.25:80";
            };
        };
      };
  };
  services.avahi = {
  enable = true;
  nssmdns = true;
  openFirewall = true;
  publish = {
    enable = true;
    addresses = true;
    domain = true;
  };
  wideArea = true;
  hostName = "proxy";
  domainName = "local";
};
			`
			},
			{
				name: "Rust Gitlab CI-CD Pipeline",
                link: 'https://gitlab.com/jaredamcdon/rust-projects/-/blob/master/ci-cd/src/main.rs',
				usage: "Automated Gitlab/GitHub to Gitea pipeline using rust binary",
				status:"Production",
				img:"./img/rust.png",
				statusText: "text-success",
				faIcon: "fa-gitlab",
				langFrame:["Rust","git", "BASH", "YAML"],
                snippetLang:"Rust",
				snippet:`
use reqwest::header::ACCEPT;
use std::env;

fn main() -> Result<(), Box<dyn std::error::Error>> {
	
	let args: Vec<String> = env::args().collect();
	let key = &args[1];
	let name = &args[2];

	let client = reqwest::blocking::Client::new();
	let mut post_string: String = "https://tea.jdogg.club/api/v1/repos/jared/".to_string();
	post_string.push_str(&name);
	post_string.push_str("/mirror-sync?access_token=");
	post_string.push_str(&key);
	client.post(&post_string)
		.header(ACCEPT, "text/html")
		.send()?;

	Ok(())
}			
`
			},
			{
				name: "CSGO-KZ-Docker",
                link: 'https://github.com/jamcdon/docker-kz-csgo',
				usage: "Automated Counterstrike Servers",
				status:"Testing",
				img:"./img/docker.png",
				statusText: "text-warning",
				faIcon: "fa-linux",
				langFrame:["BASH", "Docker"],
                snippetLang:"BASH",
				snippet:`
version=$(sed 's/.*">//g' <<< \\\\\n$(curl https://bitbucket.org/kztimerglobalteam/kztimerglobal/downloads/?tab=downloads \\\\
| grep "Full.zip" | head -1))

version=$(sed 's|</a>||g' <<< $version)
                
link="https://bitbucket.org/kztimerglobalteam/kztimerglobal/downloads/$version"
                
wget $link -O kzt.zip
unzip kzt.zip -d ./kzt
rm kzt.zip     
`
			},
			{
				name: "BASH One Time Pad",
                link: 'https://gitlab.com/jaredamcdon/bash/-/blob/master/otp.sh',
				usage: "Insecure cryptographic algorithm in BASH",
				status:"Production",
				img:"./img/bash.png",
				statusText: "text-success",
				faIcon: "fa-linux",
				langFrame:["BASH"],
                snippetLang:"BASH",
				snippet:`
function decrypt_fn {
    len=\${#cipher}
                
    keyNumeric=()
    cipherNumeric=()
    for ((i = 0 ; i < $len ; i++))
    do
        current=\${cipher:$i:1}
        cipherNumeric+=(\$\{alphabet[$current]\})
        current=\${key:$i:1}
        keyNumeric+=(\${alphabet[$current]})
    done
                
    decrypt=""
    for ((i = 0; i < $len ; i++))
    do
        decryptNum=$(expr \$\{cipherNumeric[$i]} - \$\{keyNumeric[$i]\})
        if [ $decryptNum -lt 0 ]
        then
            decrypt+=\${revAlphabet[$(expr $decryptNum + 27)]}
        else
            decrypt+=\${revAlphabet[$(expr $decryptNum % 27)]}
        fi
    done
}
`
			}
		],
	
	skills:[
		{
			tool:"Linux",
			class:{
				"fa-linux":true,
				"fab":true
			},
			stars:"████████░░",
			pct: "80%",
			specialties:["Administration","Networking"],
			learning:"IOMMU Virtualization, LXC"
		},
		{
			tool:"Python",
			class:{
				"fa-python":true,
				"fab":true
			},
			stars:"███████░░░",
			pct: "70%",
			specialties:["Web","ORM", "Networking"],
			learning:"notebooks"
		},
		{
			tool:"JavaScript",
			class:{
				"fa-js-square":true,
				"fab":true
			},
			stars:"██████░░░░",
			pct:"60%",
			specialties:["APIs","Vue"],
			learning:"Vue 3, React"
		},
		{
			tool:"SQL",
			class:{
				"fa-database":true,
				"fas":true
			},
			stars:"████████░░",
			pct: "80%",
			specialties:["Stored Procedures","ER Models", "TSQL/MySQL"],
			learning:"ORMs"
		},
		{
			tool:"Containers",
			class:{
				"fa-docker":true,
				"fab":true
			},
			stars:"████████░░",
			pct: "70%",
			specialties:["Docker", "Jails", "Kubernetes"],
			learning:"Helm"
		},
		{
			tool:"Cloud",
			class:{
				"fa-cloud":true,
				"fas":true
			},
			stars:"███████░░░",
			pct: "70%",
			specialties:["AWS","GCP","Serverless"],
			learning:"Terraform, CDK, SNS, IAM"
		},
		{
			tool:"DevOps",
			class:{
				"fa-toolbox":true,
				"fas":true
			},
			stars:"███████░░░",
			pct: "70%",
			specialties:["Cloud", "Pipelines", "GitHub Actions"],
			learning:"Drone CI, Gitea"
		},
		{
			tool:"BASH",
			class:{
				"fa-terminal":true,
				"fas":true
			},
			stars:"████████░░",
			pct: "80%",
			specialties:["Automation", "System configuration","Regex"],
			learning:"Command Line UI"
		},
		{
			tool:"Golang",
			class:{
				"fa-brands": true,
				"fa-golang": true
			},
			stars:"██████░░░░",
			pct: "60%",
			specialties:["Web, functions"],
			learning:"multithreading"
		}
	],
	school:[
		{
			name:"Georgia Institute of Technology",
			location:"Atlanta",
			startEnd:"2023-2026",
			degreeType:"Master of Science",
			degreeName:"Computer Science",
			degreeFocus:"Computing Systems",
			gpa: NaN
		},
		{
			name:"Purdue University",
			location:"Indianapolis",
			startEnd:"2018-2021",
			degreeType:"Bachelor of Science",
			degreeName:"Computer Information Technology",
			degreeFocus:"Networking Systems & Information Security",
			gpa:3.778
		}
	],
	work:[
		{
			name:"Riskalyze",
			role:"Software Engineer",
			location:"Auburn, CA",
			startEnd:"2022-",
			excerpt:`Platform Technology Engineer using Golang, PHP, Python for development and AWS/GitHub Actions for Deployments`
		},
		{
			name:"Charles Schwab",
			role:"Cloud Engineer",
			location:"Austin, TX",
			startEnd:"2022",
			excerpt:"GCP Cloud Platform Engineering"
		},
		{
			name:"NASA",
			role:"Student Trainee (IT)",
			location:"Greenbelt, MD",
			startEnd:"2021-2022",
			excerpt:"Cloud computing full stack developer, server-side scripting, project manager"
		},
		{
			name:"Rolls-Royce",
			role:"Digital Capability Co-Op",
			location:"Indianapolis, IN",
			startEnd:"2020-2021",
			excerpt:"Full-stack developer, system administrator, DevOps Engineer"
		},
		{
			name:"Indiana University",
			role:"Undergraduate Research Assistant",
			location:"Indianapolis, IN",
			startEnd:"2019-2020",
			excerpt:"Research fieldwork and analysis"
		},
		{
			name:"City of Indianapolis",
			role:"Intern",
			location:"Indianapolis, IN",
			startEnd:"2019-2019",
			excerpt:"Data collection, cleaning, and visualization intern"
		}
	],
	xpEtc:[
		{
			org:"Hudnut Scholar",
			title:"Recipient",
			excerpt:"2019 IUPUI Hudnut Scholar and City Intern"
		},
		{
			org:"IUPUI CS:GO",
			title:"President",
			excerpt:"2018-2021 President of IUPUI CS:GO esport organization"
		},
		{
			org:"AZ-900",
			title:"Microsoft Azure Fundamentals",
			excerpt:"November 13, 2021"
		},
		{
			org:"NASA Group Achievement Award",
			title:"NASA Agency Honors Award",
			excerpt:"2021"
		}
	]
},
	methods:{
		
	}
})

function delay(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

var rolodex = async function(){
	await delay(1500);
	if (app._data.role < app._data.roles.length - 1){
		app._data.role++;
	}
	else{
		app._data.role = 0;
	}
	rolodex();
}

window.onload = rolodex();
